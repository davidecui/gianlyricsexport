import codecs

import requests
import json
import urllib.parse

MAX_REQUESTS = 74
API_KEY = ""  # add api key here
API_ENDPOINT = "https://orion.apiseeds.com/api/music/lyric"
SONG_LIST_URL = "http://sfgd.altervista.org/repertorio.txt"
BLACKLIST = {"ITALIANE:", "INGLESI:", "", "Totale : 274 canzoni"}
START_FROM = 200

api_query_param = "?apikey=" + API_KEY


def get_lyrics_api_call(title, artist):
    full_query = API_ENDPOINT + "/" + urllib.parse.quote(artist) + "/" + urllib.parse.quote(title) + api_query_param
    #    print(full_query)
    api_response = requests.get(full_query)
    return api_response


def get_lyrics(index, title, artist):
    lyrics_string = get_lyrics_api_call(title, artist)
    lyrics_to_return = "NOT FOUND"
    if lyrics_string.status_code == 200:
        lyrics_json = json.loads(lyrics_string.content.decode("utf-8"))
        lyrics_to_return = lyrics_json["result"]["track"]["text"]
    return str(index) + ": === " + artist + " - " + title + " ===\n" + lyrics_to_return + "\n\n"


def split_data(x):
    data = x.split(" - ")
    # check if the split worked
    if len(data) < 2:
        data = x.split(" – ")  # it looks the same, but it is an other character

    return data


if __name__ == "__main__":
    # get the raw list and put it on a string
    response = requests.get(SONG_LIST_URL)
    song_list = response.content.decode("Windows-1252").split("\n")
    # cleanup and create a tuple array with artist and title
    song_list = [split_data(x) for x in song_list if x not in BLACKLIST]

    # open the file ("a" is for append)
    with codecs.open("lyrics.txt", "a", "utf-8") as text_file:
        lyrics = ""
        # edit START_FROM to start from a specific index
        i = 0
        for song_data in song_list[START_FROM:]:
            # get the lyrics
            lyrics = get_lyrics(START_FROM + i, song_data[1], song_data[0])
            # write on the file
            print(lyrics, file=text_file)
            i += 1
            if i == MAX_REQUESTS:
                break
